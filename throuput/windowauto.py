#encoding: utf-8
import os,time
import wx
import requests
from view import window
from wifi import *
import threading

class ExtMain(threading.Thread):
    def __init__(self,targ,callback,arg='',mode=False):
        super(ExtMain,self).__init__()
        self.targ = targ
        self.arg = arg
        self.mode = mode
        self.callback = callback
    def run(self):
        self.th2g = self.targ(self.arg)
        if self.mode is True:
            self.th5g = self.targ(self.arg+'-5G')
        self.callback(self.get_result())
    def get_result(self):
        try:
            if self.mode is True:
                return self.th2g,self.th5g
            else:
                return self.th2g,0
        except Exception:
            return None
class ExtSSID(threading.Thread):
    def __init__(self,targ,callback,arg):
        super(ExtSSID,self).__init__()
        self.targ = targ
        self.arg = arg
        self.callback = callback
    def run(self):
        self.callback(self.targ(self.arg))
    def get_result(self):
        try:
            return self.th2g
        except Exception:
            return None

class run_through(window):
    def __init__(self):
        main = wx.Frame(None, title = "广联智通科技------吞吐量测试", size = (400,300))
        super(run_through,self).__init__(main)
        # 开发者模式
        self.develop = False
    def clean(self, event):
        self.ssid_input.Clear()
    def begin(self, event):
        if self.develop is False:
            # 获取输入的扫码数值
            self.code = self.ssid_input.GetValue()
        else:
            self.code = "E4:95:6E:42:13:72"
        self.device_type = self.mode_type.GetString(self.mode_type.GetCurrentSelection())
        # 是不是双频产品如果是双频产品请在该列表里增加型号信息
        if self.device_type in ['GL-B1300','GL-AR750','GL-AR750S']:
            self.modeB = True
        else:
            self.modeB = False

        if "5G" in self.device_type:
            self.mode = "5G"
        else:
            self.mode = "2G"
        if self.code:
            runssid = ExtSSID(self.get_info,self.runtm,self.code)
            runssid.start()
            
    def get_info(self,code):
        self.get_mac_from_code(code)
        self.set_ssid_of_name()
        return self.ssid
    def runtm(self,ssid):
        self.ssid = ssid
        if self.ssid:
            runth = ExtMain(begin,self.stop_run,self.ssid,self.modeB)
            runth.start()
            self.begin_button.Disable()
    # 设置通过值
    def set_stand(self):
        if self.device_type == 'GL-B1300':
            self.stand_2g = '72'
            self.stand_5g = '433'
        elif self.device_type == 'GL-AR750':
            self.stand_2g = '72'
            self.stand_5g = '300'
        elif self.device_type == 'GL-AR750S':
            self.stand_2g = '120'
            self.stand_5g = '200'
        else:
            self.stand_2g = '72'
            self.stand_5g = '100'
    def stop_run(self,result):
        self.set_stand()
        self.begin_button.Enable()
        if self.modeB:
            self.th2g = result[0]
            self.th5g = result[1]
            self.status1.SetLabel(self.th2g)
            self.status1.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
            self.g_status1.SetLabel(self.th5g)
            self.g_status1.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
            print self.th2g
            print self.th5g
            self.check_speed()
        else:
            self.th2g = result[0]
            self.status1.SetLabel(self.th2g)
            self.status1.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
            self.check_speed()
            if self.develop is False:
                self.write_data(self.th2g,self.ddns_name)
                if self.modeB:
                    macnext = self.computmac()
                    self.get_ddns_code_from_mac(macnext)
                    self.write_data(self.th2g,self.ddns_name)
            self.ssid_input.Clear()
    # 计算下一位mac地址
    def computmac(self):
        macaddr = self.mac
        last_two = "".join(macaddr.split(":"))
        last_two_int = int(last_two,16)
        new_last_two_int = last_two_int + 1
        new_last_two = hex(new_last_two_int)
        new_last_two = new_last_two[2:-1]
        for i in range(len(new_last_two),12):
            new_last_two = '0'+str(new_last_two)
        new_addr = ""
        for item in range(1,13):
            if item % 2 == 0:
                if item == 12:
                    new_addr = new_addr + new_last_two[item-2:item]
                else:
                    new_addr =new_addr + new_last_two[item-2:item]+":"
        return new_addr.upper()
    # 比较速度值
    def check_speed(self):
        if float(self.th2g) > float(self.stand_2g):
            self.setFail()
        else:
            self.setSuccess()
        if self.modeB:
            if float(self.th5g) > float(self.stand_5g):
                self.setFail(False)
            else:
                self.setSuccess(False)
    # 通过时
    def setSuccess(self,mode=True):
        if mode:
            self.status.SetLabel('失败')
            self.status.SetForegroundColour( wx.Colour( 255, 0, 0 ) )
        else:
            self.g_status.SetLabel('失败')
            self.g_status.SetForegroundColour( wx.Colour( 255, 0, 0 ) )
    # 失败时
    def setFail(self,mode=True):
        if mode:
            self.status.SetLabel("成功")
            self.status.SetForegroundColour( wx.Colour( 0, 255, 0 ) )
        else:
            self.g_status.SetLabel("成功")
            self.g_status.SetForegroundColour( wx.Colour( 0, 255, 0 ) )

    def setTail(self,text):
        self.status.SetLabel(text)
        self.status.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
    # 解析二维码值的形式
    def split_code(self,code):
        codeList = code.split()
        if len(codeList) == 3:
            if ":" in codeList[0]:  # E4:95:6E:42:14:18 da21418 15777267fa5b8c35
                if len(codeList[1]) > len(codeList[2]):
                    return codeList[2], codeList[1], codeList[0], 3
                else:
                    return codeList[1], codeList[2], codeList[0], 3
            elif ":" in codeList[2]:  # da21418 15777267fa5b8c35 E4:95:6E:42:14:18
                if len(codeList[0]) > len(codeList[1]):
                    return codeList[1], codeList[0], codeList[2], 3
                else:
                    return codeList[0], codeList[1], codeList[2], 3
            elif ":" in codeList[1]:
                if len(codeList[0]) > len(codeList[2]):
                    return codeList[2], codeList[0], codeList[1], 3
                else:
                    return codeList[0], codeList[2], codeList[1], 3
        elif len(codeList) == 2:
            return codeList[0], codeList[1], 2
        elif len(codeList) == 1:
            return codeList[0], 1
    # 切分数值
    def get_two_par_from_code_tuple(self,code_tuple):
        code_len = code_tuple[-1]
        if code_len == 2 or code_len == 3:
            return code_tuple[0]
        elif code_len == 1:
            return get_ddns_code_from_mac(code_tuple[0])
        return False
    # 从mac地址中取code值
    def get_ddns_code_from_mac(self,mac):
        payload = {'action':'mac2ddns','mac_address':mac}
        r = requests.get("http://192.168.16.17/api.php", params=payload)
        self.ddns_name = r.json()['ddns_name']
        return r.json()['ddns_name']
    # 从code中去除mac地址
    def get_mac_from_code(self,code):
        if len(code) == 17:
            self.mac = code
            if self.develop is False:
                self.get_ddns_code_from_mac(self.mac)
            else:
                self.ddns_name = 'da21418'
            self.mac_status.SetLabel(self.mac)
            self.mac_status.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
            self.ddns_status.SetLabel(self.ddns_name)
            self.ddns_status.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
            return code
        else:
            code_tuple = self.split_code(code)
            ddns_name = self.get_two_par_from_code_tuple(code_tuple)
            payload = { 'action': 'getDeviceInfo', 'ddns_name': ddns_name }
            re = requests.get("http://192.168.16.17/api.php", params=payload)
            self.mac = re.json()[0]['mac_address']
            self.get_ddns_code_from_mac(self.mac)
            self.mac_status.SetLabel(self.mac)
            self.ddns_status.SetLabel(self.ddns_name)
        return re.json()[0]['mac_address']
    # 设置wifi的ssid值
    def set_ssid_of_name(self):
        last_3_mac = "".join(str(self.mac).split(":"))[-3:]
        ssid_info = str(self.device_type).strip()+"-"
        if "GL-CORE" == str(self.device_type).strip():
            ssid_info = "Domino-"
        ssid = ssid_info + last_3_mac.lower()
        if "Lite" in str(self.device_type).strip():
            ssid += "-NOR"
        if "16" in str(self.device_type).strip():
            ssid += "-NOR"
        if self.mode == "5G":
            ssid = ssid+"-5G"
        self.ssid = ssid
        return ssid
    # 写入数据库
    def write_data(self,speed_down,ddns_name):
        payload = { 'action': 'updateThrough', 'ddns_name': ddns_name,'throughput_rx': speed_down,'throughput_tx': speed_down }
        r = requests.get("http://192.168.16.17/api.php", params=payload)
        self.setTail(r.json())
if __name__ == "__main__":
    app = wx.App()
    main = run_through()
    main.Show()
    app.MainLoop()    