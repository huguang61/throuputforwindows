# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

import gettext
_ = gettext.gettext

###########################################################################
## Class window
###########################################################################

class window ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = _(u"广联智通吞吐量测试"), pos = wx.DefaultPosition, size = wx.Size( 800,604 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		self.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )
		self.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		
		bSizer3 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer4 = wx.BoxSizer( wx.VERTICAL )
		
		sbSizer9 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, wx.EmptyString ), wx.VERTICAL )
		
		fgSizer5 = wx.FlexGridSizer( 0, 2, 0, 0 )
		fgSizer5.SetFlexibleDirection( wx.BOTH )
		fgSizer5.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		mode_typeChoices = [ _(u"GL-B1300"), _(u"GL-AR750"), _(u"GL-AR750S") ]
		self.mode_type = wx.Choice( sbSizer9.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, mode_typeChoices, 0 )
		self.mode_type.SetSelection( 0 )
		fgSizer5.Add( self.mode_type, 0, wx.ALL, 5 )
		
		self.ssid_input = wx.TextCtrl( sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 400,25 ), 0 )
		fgSizer5.Add( self.ssid_input, 0, wx.ALL, 5 )
		
		self.delete_ssid = wx.Button( sbSizer9.GetStaticBox(), wx.ID_ANY, _(u"清除"), wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer5.Add( self.delete_ssid, 0, wx.ALL, 5 )
		
		self.begin_button = wx.Button( sbSizer9.GetStaticBox(), wx.ID_ANY, _(u"开始测试"), wx.Point( -1,-1 ), wx.Size( -1,-1 ), 0 )
		fgSizer5.Add( self.begin_button, 0, wx.ALL, 5 )
		
		self.mac_status = wx.StaticText( sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 350,-1 ), 0 )
		self.mac_status.Wrap( -1 )
		self.mac_status.SetFont( wx.Font( 20, 70, 90, 90, False, wx.EmptyString ) )
		
		fgSizer5.Add( self.mac_status, 0, wx.ALL, 5 )
		
		self.ddns_status = wx.StaticText( sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 350,-1 ), 0 )
		self.ddns_status.Wrap( -1 )
		self.ddns_status.SetFont( wx.Font( 20, 70, 90, 90, False, wx.EmptyString ) )
		
		fgSizer5.Add( self.ddns_status, 0, wx.ALL, 5 )
		
		
		sbSizer9.Add( fgSizer5, 1, wx.EXPAND, 5 )
		
		
		bSizer4.Add( sbSizer9, 1, wx.EXPAND, 5 )
		
		sbSizer3 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, _(u"label") ), wx.VERTICAL )
		
		fgSizer4 = wx.FlexGridSizer( 0, 2, 0, 0 )
		fgSizer4.SetFlexibleDirection( wx.BOTH )
		fgSizer4.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText41 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, _(u"2G:"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText41.Wrap( -1 )
		self.m_staticText41.SetFont( wx.Font( 25, 70, 90, 90, False, wx.EmptyString ) )
		self.m_staticText41.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNTEXT ) )
		
		fgSizer4.Add( self.m_staticText41, 0, wx.ALL, 5 )
		
		self.status1 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 250,-1 ), 0 )
		self.status1.Wrap( -1 )
		self.status1.SetFont( wx.Font( 55, 74, 90, 92, False, wx.EmptyString ) )
		self.status1.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNTEXT ) )
		self.status1.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		
		fgSizer4.Add( self.status1, 0, wx.ALL, 5 )
		
		self.m_staticText51 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, _(u"5G:"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText51.Wrap( -1 )
		self.m_staticText51.SetFont( wx.Font( 25, 70, 90, 90, False, wx.EmptyString ) )
		self.m_staticText51.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNTEXT ) )
		
		fgSizer4.Add( self.m_staticText51, 0, wx.ALL, 5 )
		
		self.g_status1 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 250,-1 ), 0 )
		self.g_status1.Wrap( -1 )
		self.g_status1.SetFont( wx.Font( 55, 75, 90, 92, False, wx.EmptyString ) )
		self.g_status1.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNTEXT ) )
		
		fgSizer4.Add( self.g_status1, 0, wx.ALL, 5 )
		
		
		sbSizer3.Add( fgSizer4, 1, wx.EXPAND, 5 )
		
		
		bSizer4.Add( sbSizer3, 1, wx.EXPAND, 5 )
		
		
		bSizer3.Add( bSizer4, 1, wx.EXPAND, 5 )
		
		bSizer5 = wx.BoxSizer( wx.VERTICAL )
		
		sbSizer12 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, wx.EmptyString ), wx.VERTICAL )
		
		bSizer6 = wx.BoxSizer( wx.VERTICAL )
		
		
		sbSizer12.Add( bSizer6, 1, wx.EXPAND, 5 )
		
		fgSizer2 = wx.FlexGridSizer( 0, 2, 0, 0 )
		fgSizer2.SetFlexibleDirection( wx.BOTH )
		fgSizer2.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText4 = wx.StaticText( sbSizer12.GetStaticBox(), wx.ID_ANY, _(u"2G:"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText4.Wrap( -1 )
		self.m_staticText4.SetFont( wx.Font( 25, 70, 90, 90, False, wx.EmptyString ) )
		self.m_staticText4.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNTEXT ) )
		
		fgSizer2.Add( self.m_staticText4, 0, wx.ALL, 5 )
		
		self.status = wx.StaticText( sbSizer12.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 250,-1 ), 0 )
		self.status.Wrap( -1 )
		self.status.SetFont( wx.Font( 55, 74, 90, 92, False, wx.EmptyString ) )
		self.status.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNTEXT ) )
		self.status.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		
		fgSizer2.Add( self.status, 0, wx.ALL, 5 )
		
		self.m_staticText5 = wx.StaticText( sbSizer12.GetStaticBox(), wx.ID_ANY, _(u"5G:"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )
		self.m_staticText5.SetFont( wx.Font( 25, 70, 90, 90, False, wx.EmptyString ) )
		self.m_staticText5.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNTEXT ) )
		
		fgSizer2.Add( self.m_staticText5, 0, wx.ALL, 5 )
		
		self.g_status = wx.StaticText( sbSizer12.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 250,-1 ), 0 )
		self.g_status.Wrap( -1 )
		self.g_status.SetFont( wx.Font( 55, 75, 90, 92, False, wx.EmptyString ) )
		self.g_status.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNTEXT ) )
		
		fgSizer2.Add( self.g_status, 0, wx.ALL, 5 )
		
		
		sbSizer12.Add( fgSizer2, 1, wx.EXPAND, 5 )
		
		
		bSizer5.Add( sbSizer12, 1, wx.EXPAND, 5 )
		
		
		bSizer3.Add( bSizer5, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizer3 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.delete_ssid.Bind( wx.EVT_BUTTON, self.clean )
		self.begin_button.Bind( wx.EVT_BUTTON, self.begin )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def clean( self, event ):
		event.Skip()
	
	def begin( self, event ):
		event.Skip()
	

